// const button = document.querySelector(".btn");
// button.addEventListener("click", myLocation);


// async function myLocation() {
//   try {
//     const { ip } = await (await fetch("http://api.ipify.org/?format=json")).json();
//     const locationData = await (await fetch(`http://ip-api.com/json/${ip}?fields=status,message,continent,country,countryCode,region,regionName,city,district,zip,lat,lon,timezone,isp,org,as,query`)).json();
//     displayLocationData(locationData);
//   } catch (error) {
//     console.log("Помилка:", error);
//   }
// }

// function displayLocationData(data) {
//   const locationContainer = document.querySelector(".container");
//   locationContainer.innerHTML = `
//     <p>Континент: ${data.continent}</p>
//     <p>Країна: ${data.country}</p>
//     <p>Регіон: ${data.region}</p>
//     <p>Місто: ${data.city}</p>
//     <p>Район: ${data.district}</p>
//   `;
// }
async function getIpAddress() {
  try {
      // Отримати IP адресу клієнта
      const ipResponse = await fetch('https://api.ipify.org/?format=json');
      const ipData = await ipResponse.json();
      const clientIp = ipData.ip;

      // Отримати інформацію про фізичну адресу
      const locationResponse = await fetch(`https://ip-api.com/json/${clientIp}`);
      const locationData = await locationResponse.json();

      // Вивести інформацію на сторінку
      const ipInfo = document.getElementById('ip-info');
      ipInfo.innerHTML = `
          <p>Континент: ${locationData.continent}</p>
          <p>Країна: ${locationData.country}</p>
          <p>Регіон: ${locationData.regionName}</p>
          <p>Місто: ${locationData.city}</p>
          <p>Район: ${locationData.district}</p>
      `;
  } catch (error) {
      console.error('Помилка: ', error);
  }
}

document.getElementById('find-ip-button').addEventListener('click', getIpAddress);

